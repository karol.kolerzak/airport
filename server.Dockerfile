FROM python:3.9
EXPOSE 5000 6550
WORKDIR APP

COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./server_file ./server_file
COPY ./database ./database
COPY ./server.py .
COPY ./flask_server.py .

CMD FLASK_APP=flask_server python -m flask run --host=0.0.0.0
