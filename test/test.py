import time
import unittest
from server import ServerStatus, Server
from server_file.control_tower import ControlTower
from server_file.airplane import AirPlane
from database.database_function import DB


class TestUser(unittest.TestCase):

    def test_colision(self):
        test = False

        server_status = ServerStatus()
        server_status.start()

        db = DB(server_status)

        control_tower = ControlTower(server_status, db)

        plane1 = AirPlane(100, 110, 1000, 2, 100)
        plane2 = AirPlane(100, 110, 1000, 2, 100)
        plane3 = AirPlane(100, 110, 2000, 2, 100)

        control_tower.add_airplane(plane1)
        control_tower.add_airplane(plane2)
        control_tower.add_airplane(plane3)

        control_tower.check_colision()

        if plane1 in control_tower.destroyed_airplane:
            if plane2 in control_tower.destroyed_airplane:
                if plane3 not in control_tower.destroyed_airplane:
                    test = True

        server_status.close()
        self.assertEqual(test, True)


    def test_fuel(self):
        test = False
        server_status = ServerStatus()
        server_status.start()
        db = DB(server_status)
        control_tower = ControlTower(server_status, db)

        plane1 = AirPlane(100, 110, 1000, 2, 100)
        plane2 = AirPlane(100, 110, 1000, 2, 100)

        control_tower.add_airplane(plane1)
        control_tower.add_airplane(plane2)

        plane1.start_time = time.time() - 3601
        plane2.start_time = time.time() - 500

        control_tower.check_fuel()

        if plane1 in control_tower.destroyed_airplane and plane2 not in control_tower.destroyed_airplane:
            test = True

        server_status.close()
        self.assertEqual(test, True)

if __name__ == "__main__":
    unittest.main()
