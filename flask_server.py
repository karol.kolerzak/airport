import threading
import time

from flask import Flask, jsonify
from server import Server, ServerStatus
from database.database_function import DB

app = Flask(__name__)

server_status = ServerStatus()
server_start_time = time.time()
server_status.start()
db = DB(server_status)


@app.route("/start", methods=['GET'])
def start_server():
    server = Server(server_status, db)
    thread = threading.Thread(target=server.start_server)
    thread.start()
    return jsonify({'info': f'Server start'}), 200


@app.route("/close", methods=['GET'])
def close_server():
    server_status.close()
    return jsonify({'info': f'Server closed'}), 200


@app.route("/pause", methods=['GET'])
def pause_server():
    server_status.pause()
    return jsonify({'info': f'Server paused'}), 200


@app.route("/resume", methods=['GET'])
def resume_server():
    server_status.start()
    return jsonify({'info': f'Server resumed'}), 200


@app.route("/info", methods=['GET'])
def get_all_airplanes():
    command = "SELECT * FROM airport;"
    result = db.execute(command)
    serializer_result = serializer(result)
    return jsonify(serializer_result)


@app.route('/info/<int:airplane_id>', methods=['GET'])
def get_airplane(airplane_id):
    command = "SELECT * FROM airport WHERE id=?;"
    result = db.execute(command, (airplane_id, ))
    serializer_result = serializer(result)
    return jsonify(serializer_result)


@app.route("/destroyed", methods=['GET'])
def destroyed():
    command = "SELECT * FROM airport WHERE is_destroyed=True;"
    result = db.execute(command)
    serializer_result = serializer(result)
    return jsonify(serializer_result)


@app.route("/landed", methods=['GET'])
def landed():
    command = "SELECT * FROM airport WHERE landed=True;"
    result = db.execute(command)
    serializer_result = serializer(result)
    return jsonify(serializer_result)


@app.route("/active_airplane", methods=['GET'])
def active_airplane():
    command = "SELECT airplane FROM airport WHERE is_active=True;"
    result = db.execute(command)
    list_of_plane = []
    for plane in result:
        list_of_plane.append(plane[0])

    serializer_result = {
        "active_airplane": list_of_plane
    }
    return jsonify(serializer_result)


@app.route("/sum_of_active_plane", methods=['GET'])
def sum_of_active_plane():
    command = "SELECT SUM(id) FROM airport WHERE is_active=True"
    result = db.execute(command)[0][0]
    result = {"active_airplane": result}
    return jsonify(result)


@app.route("/uptime", methods=['GET'])
def get_server_time():
    uptime = time.time() - server_start_time
    result = {"uptime": uptime}
    return jsonify(result)


##############################################


def serializer(data_to_serialize):
    serializer_data = []
    for data in data_to_serialize:
        serial = {
            "id": data[0],
            "plane_id": data[1],
            "active": data[2],
            "destroyed": data[3],
            "landed": data[4],
            "connection_time": data[5],
            "landing_time": data[6],
            "destroyed_time": data[7],
        }
        serializer_data.append(serial)

    return serializer_data


if __name__ == '__main__':
    # app.debug = True
    app.run()
