FROM python:3.9
WORKDIR APP

COPY ./client.py .
RUN mkdir "logs"

CMD python client.py
