# Airport Control System
Airport is a system that controls the airspace over an airport. Its job is to bring planes to one of two runways in a safe manner. The program, by adding multi-threading support, can control 100 planes at a time. Data is written to the base through my own Connection Pool. The airport has a REST API communication created in Flask.

## Table of contents
* [Technologies](#technologies)
* [REST API Endpoints](#rest-api-endpoints)


## Technologies
* Python
  * Threading
  * Socket
  * Logging
  * Matplotlib
* Flask
* SQLite
  * Own Connection Pool
* UnitTest

## REST API Endpoints
### Default to connect: localhost:5000

### /start

**Method:** GET

Starts the server and initializes the airport.

**Request:**
```
GET /start
```

**Response:**
```
{
  "info": "Server start"
}
```

### /close

Closes the server and stops the airport server.

**Request:**
```
GET /close
```

**Response:**
```
{
  "info": "Server closed"
}
```

### /pause

**Method:** GET

Pauses the server.

**Request:**
```
GET /pause
```

**Response:**
```
{
  "info": "Server paused"
}
```

### /resume

**Method:** GET

Resumes the server.

**Request:**
```
GET /resume
```

**Response:**
```
{
  "info": "Server resumed"
}
```

### /info

**Method:** GET

Retrieves information about all airplanes in the airport.

**Request:**
```
GET /info
```

**Response:**
```
Array of airplane objects, each containing the following fields:
- id (integer): The ID of the airplane.
- plane_id (string): Ariplane name.
- active (boolean): Indicates whether the airplane is active.
- destroyed (boolean): Indicates whether the airplane is destroyed.
- landed (boolean): Indicates whether the airplane has landed.
- connection_time (float): The time of connection.
- landing_time (float): The time of landing.
- destroyed_time (float): The time of destruction.
```

### /info/{airplane_id}

**Method:** GET

Retrieves information about a specific airplane.

**Request:**
```
GET /info/{airplane_id}
```
- Replace `{airplane_id}` in the URL with the ID of the airplane.

**Response:**
```
An airplane object with the following fields:
- id (integer): The ID of the airplane.
- plane_id (string): Ariplane name.
- active (boolean): Indicates whether the airplane is active.
- destroyed (boolean): Indicates whether the airplane is destroyed.
- landed (boolean): Indicates whether the airplane has landed.
- connection_time (float): The time of connection.
- landing_time (float): The time of landing.
- destroyed_time (float): The time of destruction.
```

### /destroyed

**Method:** GET

Retrieves information about all destroyed airplanes.

**Request:**
```
GET /destroyed
```

**Response:**
```
Array of destroyed airplane objects, each containing the same fields as described in the /info endpoint.
```

### /landed

**Method:** GET

Retrieves information about all landed airplanes.

**Request:**
```
GET /landed
```

**Response:**
```
Array of landed airplane objects, each containing the same fields as described in the /info endpoint.
```

### /active_airplane

**Method:** GET

Retrieves information about all active airplanes in the airport.

**Request:**
```
GET /active_airplane
```

**Response:**
```
{
  "active_airplane": [list_of_plane]
}
```

### /sum_of_active_plane

**Method:** GET

Retrives information about amount of active aircraft


**Request:**
```
GET /sum_of_active_plane
```

**Response:**
```
{
  "active_airplane": result
}
```

### /uptime

**Method:** GET

Retrieves the uptime of the server in seconds.

**Request:**
```
GET /uptime
```

**Response:**
```
{
  "uptime": uptime
}
```