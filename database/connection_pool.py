import time
import threading
import sqlite3
from sqlite3 import Error


class ConnectionPool:

    def __init__(self, min, max, server_status):
        self.min = int(min)
        self.max = int(max)
        self.pool = []
        self.connections_in_use = []
        self.lock = threading.Lock()
        self.server_status = server_status


        for _ in range(self.min):
            self.pool.append(self.create_connection())

    def start_work(self):
        start_work_thread = threading.Thread(target=self.control_connection, args=())
        start_work_thread.start()

    def control_connection(self):
        while self.server_status.status:
            self.check_pool()
            self.remove_old_connection()
            time.sleep(1)

    def create_connection(self):
        conn = None
        try:
            conn = sqlite3.connect(r"airport.db", check_same_thread=False)
        except Error as error:
            print(error)
        finally:
            if conn:
                active = True
                return [conn, active]

    def check_pool(self):
        total_connection = len(self.pool) + len(self.connections_in_use)
        if total_connection < self.max:
            len_pool = len(self.pool)
            if len_pool < self.min:
                self.pool.append(self.create_connection())

    def getconn(self):
        self.lock.acquire()
        if len(self.pool) > 0:
            conn = self.pool[0]
            self.connections_in_use.append(conn)
            self.pool.remove(conn)
            conn = conn[0]
        else:
            conn = None
            print("Max connections in use")

        self.lock.release()
        return conn

    def putconn(self, connection):
        i = self.connections_in_use.index([connection, True])
        self.connections_in_use[i] = [connection, False]


    def remove_old_connection(self):
        for connection in self.connections_in_use:
            if connection[1] == False:
                self.connections_in_use.remove(connection)
