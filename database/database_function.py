from sqlite3 import Error
from database.connection_pool import ConnectionPool


class DB:
    def __init__(self, server_status):
        self.connection_pool = ConnectionPool(10, 105, server_status)
        self.connection_pool.start_work()
        self.create_database()

    def create_database(self):
        try:
            connection = self.connection_pool.getconn()
            cursor = connection.cursor()
            command = '''CREATE TABLE IF NOT EXISTS airport (
                            id INTEGER PRIMARY KEY,
                            airplane VARCHAR(100) NOT NULL,
                            is_active BOOLEAN DEFAULT True,
                            is_destroyed BOOLEAN DEFAULT False,
                            landed BOOLEAN DEFAULT False,
                            connection_time TEXT DEFAULT (datetime('now')),
                            landing_time TEXT DEFAULT '',
                            destroyed_time TEXT DEFAULT '',
                            x INTEGER DEFAULT NULL,
                            y INTEGER DEFAULT NULL,
                            z INTEGER DEFAULT NULL
                        );'''


            cursor.execute(command)
            info = 'Database exist.'
            reply = (info, True)
            self.connection_pool.putconn(connection)
        except Error as error:
            reply = (error, False)
        finally:
            return reply

    def execute(self, query, parameters=()):
        try:
            connection = self.connection_pool.getconn()
            cursor = connection.cursor()
            cursor.execute(query, parameters)
            reply = cursor.fetchall()
            connection.commit()
            self.connection_pool.putconn(connection)
        except Error as error:
            info = error
            reply = info
        finally:
            return reply
