import threading

class Runway:

    def __init__(self, x, y, path):
        self.x = x
        self.y = y
        self.status = 0
        self.airplane = None
        self.path = path
        self.lock = threading.Lock()

    def block(self, airplane):
        self.lock.acquire()
        self.status = 1
        self.airplane = airplane
        self.lock.release()

    def release(self):
        self.lock.acquire()
        self.status = 0
        self.airplane = None
        self.lock.release()
