import threading
import datetime
import time
import matplotlib.pyplot as plt
from server_file.circle import Circle
from server_file.runway import Runway


class ControlTower:

    def __init__(self, server_status, db):
        self.airplanes = []
        self.circles = []
        self.runways = []
        self.destroyed_airplane = []

        self.server_status = server_status
        self.db = db

        self.create_circle()
        self.create_runway()

        self.lock = threading.Lock()

        self.date_start = datetime.datetime.now()

        # thread = threading.Thread(target=self.airplane_monitor, args=())
        # thread.start()

        self.start_work()

    def start_work(self):
        thread = threading.Thread(target=self.control_air, args=())
        thread.start()

    def finish_work(self):
        self.lock.acquire()
        self.server_status.status = False
        self.lock.release()

    def control_air(self):
        while self.server_status.status:

            while self.server_status.status == 'pause':
                time.sleep(1)

            self.control_airspace()
            self.check_colision()
            self.check_fuel()
            self.check_airplanes_status()
            time.sleep(1)

    def airplane_monitor(self):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        while self.server_status.status:

            while self.server_status.status == 'pause':
                time.sleep(1)

            ax.cla()
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')

            ax.set_xlim(xmin=-5000, xmax=5000)
            ax.set_ylim(ymin=-5000, ymax=5000)
            ax.set_zlim(zmin=0, zmax=5000)

            for airplane in self.airplanes:
                ax.scatter(airplane.x, airplane.y, airplane.z)

            plt.draw()
            plt.pause(1)

    def control_airspace(self):
        airplane = self.select_airplane_to_landing()
        if airplane is not None:
            for runway in self.runways:
                if runway.status == 0:
                    airplane.set_permission_to_landing()
                    runway.block(airplane)
                    airplane.set_runway(runway)
                    break

    def check_colision(self):
        for airplane in self.airplanes:
            for airplane_to_compare in self.airplanes:
                if airplane == airplane_to_compare:
                    continue

                if self.checking_distance_between_plane(airplane, airplane_to_compare):
                    self.destroyed_airplane.append(airplane)
                    self.destroyed_airplane.append(airplane_to_compare)

                    command = """UPDATE airport SET is_destroyed=1, destroyed_time=datetime('now')
                                             where airplane in(?, ?)"""

                    parameters = (id(airplane), id(airplane_to_compare))
                    self.db.execute(command, parameters)

                    for runway in self.runways:
                        if runway.airplane == airplane or runway.airplane == airplane_to_compare:
                            runway.release()

    def checking_distance_between_plane(self, airplane, airplane_to_compare):
        if abs(airplane.x - airplane_to_compare.x) <= 10:
            if abs(airplane.y - airplane_to_compare.y) <= 10:
                if abs(airplane.z - airplane_to_compare.z) <= 10:
                    return True
        return False



    def check_fuel(self):
        for airplane in self.airplanes:
            result = time.time() - airplane.start_time
            if result > 3600:
                self.destroyed_airplane.append(airplane)
                for runway in self.runways:
                    if runway.airplane == airplane:
                        runway.release()

    def create_circle(self):
        for z in range(2000, 5000, 30):
            self.circles.append(Circle(z))

    def create_runway(self):
        self.runways.append(Runway(100, 0, (3000, 0, 1000)))
        self.runways.append(Runway(-100, 0, (-3000, 0, 1000)))

    def add_airplane(self, airplane):
        self.airplanes.append(airplane)

    def remove_airplane(self, airplane):
        self.airplanes.remove(airplane)

    def select_circle(self, z):
        result = self.circles[-1]

        for circle in self.circles:
            if circle.status == 0:
                temp = abs(circle.z - z)
                if temp < abs(result.z - z):
                    result = circle

        result.status = 1
        return result

    def select_airplane_to_landing(self):
        for airplane in self.airplanes:
            if airplane.status == 1 and airplane.permission_to_land is False:
                return airplane

    def check_airplanes_status(self):
        for airplane in self.airplanes:
            if airplane.status == 5:
                self.airplanes.remove(airplane)
