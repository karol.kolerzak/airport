import time
import threading
import math


V_X = 150
V_Y = 150
V = round(math.sqrt(pow(V_X, 2) + pow(V_Y, 2)))  # m/s

class AirPlane:

    def __init__(self, x, y, z, status, circle):
        self.x = x
        self.y = y
        self.z = z
        self.status = status
        self.start_time = time.time()
        self.circle = circle
        self.permission_to_land = False
        self.runway = None
        self.lock = threading.Lock()

    def update(self, x=None, y=None, z=None, status=None):
        self.lock.acquire()

        if x is not None:
            self.x = x
        if y is not None:
            self.y = y
        if z is not None:
            self.z = z
        if status is not None:
            self.status = status

        self.lock.release()

    def set_permission_to_landing(self):
        self.permission_to_land = True

    def calculate_distance_to_circle(self, x, y, z):

        if x >= 0 and y >= 0:  # I piece
            distance = math.sqrt(pow(x - self.circle.x, 2) + pow((y - self.circle.y), 2)) - self.circle.radius
        elif x < 0 and y >= 0:  # II piece
            distance = math.sqrt(pow(self.circle.x - x, 2) + pow((y - self.circle.y), 2)) - self.circle.radius
        elif x < 0 and y < 0:  # III piece
            distance = math.sqrt(pow(self.circle.x - x, 2) + pow((self.circle.y - y), 2)) - self.circle.radius
        elif x >= 0 and y < 0:  # IV
            distance = math.sqrt(pow(x - self.circle.x, 2) + pow((self.circle.y - y), 2)) - self.circle.radius

        if distance != 0:
            ratio = self.circle.radius / (distance + self.circle.radius)
        else:
            ratio = 0

        delta_x = round(x * ratio)
        delta_y = round(y * ratio)

        return delta_x, delta_y, z

    def move_to_circle(self, x, y, z, status):

        delta_x, delta_y, delta_z = self.calculate_distance_to_circle(x, y, z)

        if x >= 0:
            x = x - V_X if abs(x - delta_x) > 100 else delta_x
        else:
            x = x + V_X if abs(x - delta_x) > 100 else delta_x

        if y >= 0:
            y = y - V_Y if abs(y - delta_y) > 100 else delta_y
        else:
            y = y + V_Y if abs(y - delta_y) > 100 else delta_y

        if delta_x == x and delta_y == y:
            status = 1

        new_airplane_data = {
            "x": x,
            "y": y,
            "z": z,
            "status": status,
        }

        return new_airplane_data

    def rotation(self, x, y, z, status, angle):
        if self.permission_to_land:
            self.status = 2

        road = 2 * 3.14 * self.circle.radius
        steps = round(road / V)

        x = round(self.circle.radius * math.cos(angle))
        y = round(self.circle.radius * math.sin(angle))

        angle += 2 * math.pi / steps

        new_airplane_data = {
            "x": x,
            "y": y,
            "z": z,
            "status": self.status,
        }

        return new_airplane_data, angle

    def set_position_on_the_circle(self, new_airplane_data):
        x = new_airplane_data['x']
        y = new_airplane_data['y']
        z = new_airplane_data['z']
        status = new_airplane_data['status']

        # calculate distance to path
        x1 = self.runway.path[0]
        y1 = self.runway.path[1]

        x_delta = abs(x1 - x)
        y_delta = abs(y1 - y)

        if x_delta < V_X:
            x = x1

        if y_delta < V_Y:
            y = y1

        if x == x1 and y == y1:
            status = 3

        new_airplane_data = {
            "x": x,
            "y": y,
            "z": z,
            "status": status,
        }

        return new_airplane_data

    def calculate_vz_velocity(self, x, x1, z, z1):
        delta_x = x1 - x
        delta_z = z1 - z
        v_z = round(delta_x / V) if delta_x > delta_z else V
        return v_z

    def landing(self, x, y, z, status):
        x0 = self.runway.x
        z0 = 0

        delta_x = abs(x - x0)
        delta_z = z - z0

        v_z = self.calculate_vz_velocity(x0, x, z0, z)

        if delta_x >= V_X:
            if self.x < 0:
                x += V_X
            else:
                x -= V_X
        else:
            if self.x < 0:
                x += delta_x
            else:
                x -= delta_x

        if delta_z > v_z:
            z -= v_z
        else:
            z -= delta_z

        if x == x0 and z == z0:
            status = 5

        new_airplane_data = {
            "x": x,
            "y": y,
            "z": z,
            "status": status,
        }

        return new_airplane_data

    def set_runway(self, runway):
        self.runway = runway

    def remove_runway(self, runway):
        self.runway = None
