import random
import socket
import time
import logging
import json

SIZE = 64
HOST = "127.0.0.1"
PORT = 6550
FORMAT = 'utf-8'

MIN_HEIGHT = 2000
MAX_HEIGHT = 5000


class AirPlane:

    def __init__(self):
        self.x, self.y = self.init_random_position()
        self.z = random.randint(MIN_HEIGHT, MAX_HEIGHT)
        self.start_time = time.time()  # fuel
        self.status = 0

        self.airplane_data = {
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "status": self.status,
        }

        self.x_list = []
        self.y_list = []
        self.z_list = []

        self.x_list.append(self.x)
        self.y_list.append(self.y)
        self.z_list.append(self.z)

    def init_random_position(self):
        positions = [(-5000, random.randint(-5000, 5000)),
                     (5000, random.randint(-5000, 5000)),
                     (random.randint(-5000, 5000), 5000),
                     (random.randint(-5000, 5000), -5000)]
        x, y = random.choice(positions)
        return x, y

    def update_data(self, airplane_data):
        airplane_data = airplane_data

        self.x = airplane_data['x']
        self.y = airplane_data['y']
        self.z = airplane_data['z']
        self.status = airplane_data['status']

        self.x_list.append(self.x)
        self.y_list.append(self.y)
        self.z_list.append(self.z)

        self.airplane_data = {
            "x": self.x,
            "y": self.y,
            "z": self.z,
            "status": self.status,
        }

    def send(self, conn, command):
        command = str(command)
        command = command.encode(FORMAT)
        command_len = str(len(command)).encode(FORMAT)
        command_to_send = command_len + b' ' * (SIZE - len(command_len))
        command_to_send += command
        conn.send(command_to_send)
        return self.recive(conn)

    def recive(self, conn):
        reply_len = conn.recv(SIZE).decode(FORMAT)
        if len(reply_len) > 0:
            reply_len = int(reply_len)
            reply = conn.recv(reply_len).decode(FORMAT)
            reply_as_json = json.loads(reply)
            return reply_as_json


########################

def airplane_logging(plane):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(f"Airplane")
    handler = logging.FileHandler(f'./logs/{plane}.logs', mode='w')
    logger.addHandler(handler)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    return logger


def main():
    plane = AirPlane()
    logger = airplane_logging(id(plane))
    logger.info(f'Plane: {plane} was created')

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
            conn.connect((HOST, PORT))
            while True:
                airplane_data_to_send = json.dumps(plane.airplane_data)
                new_airplane_data = plane.send(conn, airplane_data_to_send)

                if new_airplane_data == "Sorry, We have to much plane. You must go to other airport":  # need to create other status
                    break

                plane.update_data(new_airplane_data)

                if plane.status >= 1:
                    logger.info(f"{plane}'s position: {plane.x, plane.y, plane.z}, status: {plane.status}")

                if plane.status == 5:
                    airplane_data_to_send = json.dumps(plane.airplane_data)
                    plane.send(conn, airplane_data_to_send)
                    plane.update_data(new_airplane_data)
                    logger.info(
                        f"{plane}'s position: {plane.x, plane.y, plane.z}, status: {plane.status}, CONNECTION CLOSED")
                    break

    except (ConnectionResetError, ConnectionRefusedError, AttributeError) as error:
        print("The connection to the control tower has been lost. Start the emergency procedure! You must go to the "
              "other airport!")
        conn.close()


if __name__ == "__main__":
    main()
