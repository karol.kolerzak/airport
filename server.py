import socket
import threading
import math
import time
import logging
import json

from server_file.airplane import AirPlane
from server_file.control_tower import ControlTower

from database.database_function import DB


SIZE = 64
PORT = 6550
HOST = "127.0.0.1"
FORMAT = 'utf-8'

MAX_AIRPLANE = 100


class ServerStatus:
    def __init__(self):
        self.status = False
        self.lock = threading.Lock()

    def start(self):
        self.lock.acquire()
        self.status = True
        self.lock.release()

    def close(self):
        self.lock.acquire()
        self.status = False
        self.lock.release()

    def pause(self):
        self.lock.acquire()
        self.status = 'pause'
        self.lock.release()


# STATUS
# 0 - move to Circle
# 1 - Move around the circle
# 2 - Move to the landing path
# 3 - Landing
# 4 - in the future to help landing
# 5- Airplane landed

class Server:

    def __init__(self, server_status, db):
        self.semaphore = threading.Semaphore(MAX_AIRPLANE)
        self.lock = threading.Lock()

        self.server_status = server_status
        self.db = db

        self.control_tower = ControlTower(self.server_status, self.db)
        self.control_tower.start_work()

    def _server(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.settimeout(10)
            s.bind((HOST, PORT))
            s.listen()

            while self.server_status.status:
                while self.server_status.status == 'pause':
                    time.sleep(1)
                try:
                    conn, addr = s.accept()
                except socket.timeout:
                    continue

                if self.semaphore.acquire(blocking=False):
                    thread = threading.Thread(target=self._handle_client, args=(conn, addr))
                    thread.start()
                else:
                    self._send(conn, "Sorry, We have to much plane. You must go to other airport")
                    conn.close()

    def start_server(self):
        self.lock.acquire()
        self._server()
        self.lock.release()

    def _recive(self, conn):
        while True:
            message_length = conn.recv(SIZE).decode(FORMAT)
            if message_length:
                message_size = int(message_length)
                airplane_data = conn.recv(message_size).decode(FORMAT)
                return airplane_data

    def _send(self, conn, result):
        message_to_send = self._encode_reply(result)
        conn.send(message_to_send)

    def _encode_reply(self, result):
        result = result.encode(FORMAT)
        len_answear = str(len(result)).encode(FORMAT)
        len_answear += b' ' * (SIZE - len(len_answear))
        return len_answear + result

    def _processing_airplane_position(self, airplane_data):
        airplane_data = json.loads(airplane_data)
        x = airplane_data['x']
        y = airplane_data['y']
        z = airplane_data['z']
        status = airplane_data['status']

        return x, y, z, status

    def _handle_client(self, conn, addr):
        try:
            airplane_data = self._recive(conn)
            x, y, z, status = self._processing_airplane_position(airplane_data)
            circle = self.control_tower.select_circle(z)
            airplane = AirPlane(x, y, z, status, circle)

            command = "INSERT INTO airport (airplane) VALUES (?);"
            parameters = ((id(airplane)),)
            self.db.execute(command, parameters)

            self.control_tower.add_airplane(airplane)

            while self.server_status.status:

                self.update_record_in_database(airplane)

                while self.server_status.status == 'pause':
                    time.sleep(1)

                if airplane.status == 0:  # move to circle
                    new_airplane_data = airplane.move_to_circle(x, y, z, status)
                    angle = math.atan2(y - 0, x - 0)

                elif airplane.status == 1:  # move around
                    new_airplane_data, angle = airplane.rotation(x, y, z, status, angle)

                elif airplane.status == 2:  # move to landing path
                    new_airplane_data, angle = airplane.rotation(x, y, z, status, angle)
                    new_airplane_data = airplane.set_position_on_the_circle(new_airplane_data)

                elif airplane.status == 3: #landing
                    new_airplane_data = airplane.landing(x, y, z, status)

                elif airplane.status == 5: #Landed
                    command = "UPDATE airport SET landed=1, landing_time=datetime('now') where airplane=?"
                    parameters = ((id(airplane)),)
                    self.db.execute(command, parameters)

                    airplane.runway.release()
                    new_airplane_data = json.dumps(new_airplane_data)
                    self._send(conn, new_airplane_data)
                    airplane_data = self._recive(conn)
                    x, y, z, status = self._processing_airplane_position(airplane_data)
                    airplane.update(x, y, z, status)
                    break

                #send
                new_airplane_data_to_send = json.dumps(new_airplane_data)
                self._send(conn, new_airplane_data_to_send)

                #revice
                airplane_data = self._recive(conn)

                #logic
                x, y, z, status = self._processing_airplane_position(airplane_data)
                airplane.update(x, y, z, status)
                time.sleep(1)

        except ConnectionResetError:
            print(f"Connection with {id(airplane)} airplane has been closed")

    def update_record_in_database(self, airplane):
        command = "UPDATE airport SET landed=1, landing_time=datetime('now'), x=?, y=?, z=? where airplane=?"
        parameters = (airplane.x, airplane.y, airplane.z, (id(airplane)), )
        self.db.execute(command, parameters)
###########################################################################################################

def airplane_logging(plane):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger(f"Airplane")
    handler = logging.FileHandler(f'./logs/{plane}.logs', mode='w')
    logger.addHandler(handler)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    return logger


def main():
    server_status = ServerStatus()
    server_status.start()

    db = DB(server_status)

    server = Server(server_status, db)
    thread = threading.Thread(target=server.start_server)
    thread.start()
    print("Start server")


if __name__ == "__main__":
    main()